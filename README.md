﻿# C# Windows Test Project with SQL Lite Encryption

I had problems getting this to work without more explicit instructions. Once I figured out the Secret Sauce, I used this project to validate the process.

**Important:**  NuGets project names are case-sensitive, and there are old, projects with similar names. My solution is to only use **NuGet Package Manager Console**.

This uses **Frank Krueger's** [sqlite-net](https://github.com/praeclarum/sqlite-net) with **Eric Sink's** the encryption library [SQLCipher](https://github.com/ericsink/SQLitePCL.raw/wiki/How-to-use-SQLCipher-with-SQLite-net).

All my testing was completed with VS 2017 on Windows 7 and Windows 10.

### For updating the default project

1. To clean out any older, similar packages:

     ```
     Get-Package -Filter sqlite | ForEach-Object { Uninstall-Package $_.Id }
     ```

    You may need to repeat this.
2. To confirm you have no lingering packages: 

     ```
     Get-Package -Filter sqlite
     ```

3. Check the Solutions Packages folder and remove anything that could still be present and clean manually. _This may not be required._
4. Edit the project's **packages.config** file and remove anything that could conflict. _This may not be required.__
5. Install the packages using PM CLI in this order:

    ```
    Install-Package sqlite-net-sqlcipher
    Install-Package SQLitePCLRaw.bundle_sqlcipher
    ```

7. Run project and test


### For updating the non-default project

1. Clean-out

     ```
     Get-Package -Filter sqlite -ProjectName foo | ForEach-Object { Uninstall-Package $_.Id -ProjectName foo }
     ```

    You may need to repeat this.
    You may need to repeat this.
2. To confirm you have no lingering packages: 

     ```
     Get-Package -Filter sqlite -ProjectName foo 
     ```

3. Check the Solutions Packages folder and remove anything that could still be present and clean manually. _This may not be required._
4. Edit the project's **packages.config** file and remove anything that could conflict. _This may not be required._
5. Install the packages using PM CLI in this order:

    ```
    Install-Package sqlite-net-sqlcipher -ProjectName foo 
    Install-Package SQLitePCLRaw.bundle_sqlcipher -ProjectName foo 
    ```

7. Run project and test
