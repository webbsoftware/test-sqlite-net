﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SQLite;

namespace sqlite_test05
{
    public class Settings
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public string ItemName { get; set; }
        public string ItemValue { get; set; }
    }


    class test05
    {
        static void Main(string[] args)
        {
            SQLitePCL.Batteries_V2.Init();
            string testItemKey = "DownloadFolder";

            // non-encrypted db
            string dbName = "Settings.sqlite";
            if (File.Exists(dbName)) File.Delete(dbName);

            var db1 = new SQLiteConnection(dbName);
            db1.CreateTable<Settings>();
            var Id1 = db1.Insert(new Settings()
            {
                ItemName = testItemKey,
                ItemValue = @"c:\temp"
            });
            db1.Close();
            db1 = new SQLiteConnection(dbName);
            var results = db1.Table<Settings>().Where(v => v.ItemName.Equals(testItemKey));
            foreach (var item in results)
                Console.WriteLine("Item: " + item.ItemValue);
            Console.ReadLine();
            db1.Close();

            // encrypted db
            string dbNameEnc = "SettingsEnc.sqlite";
            if (File.Exists(dbNameEnc)) File.Delete(dbNameEnc);

            string dbEncryptKey = "12345678901234567890123456789012";
            var db2 = new SQLiteConnection(dbNameEnc, key: dbEncryptKey);
            db2.CreateTable<Settings>();
            var Id2 = db2.Insert(new Settings()
            {
                ItemName = testItemKey,
                ItemValue = @"c:\temp"
            });
            db2.Close();
            db2 = new SQLiteConnection(dbNameEnc, key: dbEncryptKey);
            results = db2.Table<Settings>().Where(v => v.ItemName.Equals(testItemKey));
            foreach (var item in results)
                Console.WriteLine("Item: " + item.ItemValue);
            Console.ReadLine();
        }
    }
}
